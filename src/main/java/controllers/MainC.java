/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.is2t1.app.views.ContactFrame;
import com.is2t1.app.views.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;

/**
 *
 * @author Sistemas-11
 */
public class MainC implements ActionListener {
    static int windowsCount = 0;
    private MainFrame frame;
    private JFileChooser fc; 
    
    public MainC() {
        frame = new MainFrame();
        fc = new JFileChooser();
    }
    public MainC(MainFrame f){
        frame = f;
        fc = new JFileChooser();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "new":
                ShowContactFrame();
                break;
            case "exit":
                System.exit(0);
        }
    }

    private void ShowContactFrame() {
        ContactFrame CF = new ContactFrame();
        frame.desktop.add(CF);
        CF.setVisible(true);
    }
    
}
